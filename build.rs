#![allow(unused_variables)]

use std::io::prelude::*;
use std::io::BufReader;
use std::path::{Path, PathBuf};
use std::process::Command;

use semver::Version;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

fn qmake_query(var: &str) -> Result<String, std::io::Error> {
    let qmake = std::env::var("QMAKE").unwrap_or("qmake".to_string());
    let output = Command::new(qmake).args(&["-query", var]).output()?;
    if !output.status.success() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!(
                "qmake returned with error:\n{}\n{}",
                std::str::from_utf8(&output.stderr).unwrap_or_default(),
                std::str::from_utf8(&output.stdout).unwrap_or_default()
            ),
        ));
    }
    Ok(std::str::from_utf8(&output.stdout).expect("UTF-8 conversion failed").trim().to_string())
}

fn open_core_header(file: &str, qt_include_path: &str, qt_library_path: &str) -> BufReader<std::fs::File> {
    let cargo_target_os = std::env::var("CARGO_CFG_TARGET_OS").unwrap();

    let mut path = PathBuf::from(qt_include_path);
    path.push("QtCore");
    path.push(file);
    if cargo_target_os == "macos" {
        if !path.exists() {
            path = Path::new(qt_library_path).join("QtCore.framework/Headers");
            path.push(file);
        }
    }
    let f = std::fs::File::open(&path).expect(&format!("Cannot open `{:?}`", path));
    BufReader::new(f)
}

fn detect_qreal_size(qt_include_path: &str, qt_library_path: &str) {
    const CONFIG_HEADER: &'static str = "qconfig.h";
    let b = open_core_header(CONFIG_HEADER, qt_include_path, qt_library_path);

    // Find declaration of QT_COORD_TYPE
    for line in b.lines() {
        let line = line.expect("UTF-8 conversion failed for qconfig.h");
        if line.contains("QT_COORD_TYPE") {
            if line.contains("float") {
                println!("cargo:rustc-cfg=qreal_is_float");
                return;
            } else {
                panic!("QT_COORD_TYPE with unknown declaration {}", line);
            }
        }
    }
}

fn detect_version_from_header(qt_include_path: &str, qt_library_path: &str) -> String {
    const VERSION_HEADER: &'static str = "qtcoreversion.h";
    let b = open_core_header(VERSION_HEADER, qt_include_path, qt_library_path);

    // Find declaration of QTCORE_VERSION_STR
    for line in b.lines() {
        let line = line.expect("UTF-8 conversion failed for qtcoreversion.h");
        if line.contains("QTCORE_VERSION_STR") {
            return line.split('\"').nth(1).expect("Parsing QTCORE_VERSION_STR").into();
        }
    }
    panic!("Could not detect Qt version from include paths")
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

fn main() {

    let cargo_target_os = std::env::var("CARGO_CFG_TARGET_OS").unwrap();
    let cargo_target_env = std::env::var("CARGO_CFG_TARGET_ENV").unwrap();

    println!("cargo:rerun-if-env-changed=QT_INCLUDE_PATH");
    println!("cargo:rerun-if-env-changed=QT_LIBRARY_PATH");

    let (qt_version, qt_include_path, qt_library_path) = match (
        std::env::var("QT_INCLUDE_PATH").ok().filter(|x| !x.is_empty()),
        std::env::var("QT_LIBRARY_PATH").ok().filter(|x| !x.is_empty()),
    ) {
        (Some(qt_include_path), Some(qt_library_path)) => {
            let qt_version = detect_version_from_header(&qt_include_path, &qt_library_path);
            (qt_version, qt_include_path, qt_library_path)
        }
        (None, None) => {
            let qt_version = match qmake_query("QT_VERSION") {
                Ok(v) => v,
                Err(_err) => {
                    #[cfg(feature = "required")]
                    panic!(
                        "Error: Failed to execute qmake. Make sure 'qmake' is in your path!\n{:?}",
                        _err
                    );
                    #[cfg(not(feature = "required"))]
                    {
                        println!("cargo:rerun-if-env-changed=QMAKE");
                        println!("cargo:rustc-cfg=no_qt");
                        println!("cargo:FOUND=0");
                        return;
                    }
                }
            };
            let qt_include_path = qmake_query("QT_INSTALL_HEADERS").unwrap();
            let qt_library_path = qmake_query("QT_INSTALL_LIBS").unwrap();
            println!("cargo:rerun-if-env-changed=QMAKE");
            (qt_version, qt_include_path, qt_library_path)
        }
        (Some(_), None) | (None, Some(_)) => {
            panic!("QT_INCLUDE_PATH and QT_LIBRARY_PATH env variable must be either both empty or both set.")
        }
    };

    let qt_version = qt_version
        .parse::<Version>()
        .expect("Parsing Qt version failed");

    let conda_target_env = std::env::var("CONDA_PREFIX").unwrap_or_default();

    let mut config = cpp_build::Config::new();

    if cargo_target_os == "macos" {
        config.flag("-F");
        config.flag(&qt_library_path);
        config.include(format!("{}/QtCore.framework/Headers", &qt_library_path));
        config.include(format!("{}/QtGui.framework/Headers", &qt_library_path));
        config.include(format!("{}/QtQml.framework/Headers", &qt_library_path));
        config.include(format!("{}/QtQuick.framework/Headers", &qt_library_path));
        config.include(format!("{}/QtQuick3D.framework/Headers", &qt_library_path));
        config.include(format!("{}/QtWidgets.framework/Headers", &qt_library_path));
        config.include(format!("{}/QtOpenGL.framework/Headers", &qt_library_path));
    }

    if cargo_target_os == "linux" {
        config.include(format!("{}/QtCore", &qt_include_path));
        config.include(format!("{}/QtGui", &qt_include_path));
        config.include(format!("{}/QtQml", &qt_include_path));
        config.include(format!("{}/QtQuick", &qt_include_path));
        config.include(format!("{}/QtQuick3D", &qt_include_path));
        config.include(format!("{}/QtOpenGL", &qt_include_path));
    }

    detect_qreal_size(&qt_include_path, &qt_library_path);

    if qt_version >= Version::new(6, 0, 0) {
        config.flag_if_supported("-std=c++17");
        config.flag_if_supported("/std:c++17");
        config.flag_if_supported("/Zc:__cplusplus");
    }
    config
        .include(&qt_include_path)
        .include("lib")
        .include(format!("{}/include/vtk-9.1", &conda_target_env))
        .build("src/main.rs");

    println!("cargo:VERSION={}", &qt_version);
    println!("cargo:LIBRARY_PATH={}", &qt_library_path);
    println!("cargo:INCLUDE_PATH={}", &qt_include_path);
    println!("cargo:FOUND=1");

    let macos_lib_search = if cargo_target_os == "macos" { "=framework" } else { "" };
    let vers_suffix = if cargo_target_os == "macos" {
        "".to_string()
    } else {
        qt_version.major.to_string()
    };

    let debug = std::env::var("DEBUG").ok().map_or(false, |s| s == "true");
    let windows_dbg_suffix = if debug && (cargo_target_os == "windows") && (cargo_target_env == "msvc") {
        println!("cargo:rustc-link-lib=msvcrtd");
        "d"
    } else {
        ""
    };

    // if std::env::var("CARGO_CFG_TARGET_FAMILY").as_ref().map(|s| s.as_ref()) == Ok("unix") {
    //     println!("cargo:rustc-cdylib-link-arg=-Wl,-rpath,{}", &qt_library_path);
    // }

    println!("cargo:rustc-link-search{}={}", macos_lib_search, &qt_library_path);

    let link_lib = |lib: &str| {
        println!(
            "cargo:rustc-link-lib{search}=Qt{vers}{lib}{suffix}",
            search = macos_lib_search,
            vers = vers_suffix,
            lib = lib,
            suffix = windows_dbg_suffix
        )
    };
    link_lib("Core");
    link_lib("Gui");
    link_lib("Quick");
    link_lib("Quick3D");
    link_lib("Qml");
    link_lib("QuickControls2");
    link_lib("Multimedia");
    link_lib("MultimediaWidgets");
    link_lib("Sql");
    link_lib("Widgets");
    link_lib("OpenGL");

    if conda_target_env != "" {
        println!("cargo:rustc-link-search=native={}/lib", conda_target_env);
    }

    #[cfg(target_os = "macos")]
    println!("cargo:rustc-link-search=native=/opt/homebrew/lib"); // TODO: Fixme
    println!("cargo:rustc-link-search=native=/usr/local/lib");    // TODO: Fixme

    #[cfg(target_os = "macos")]
    println!("cargo:rustc-link-lib=framework=OpenGL");

    if cargo_target_os == "linux" {
        println!("cargo:rustc-link-lib=GL");
  //      println!("cargo:rustc-link-lib=GLU");
  //      println!("cargo:rustc-link-lib=GLX");
        println!("cargo:rustc-link-lib=X11");
        println!("cargo:rustc-link-lib=Xt");
    }
    // println!("cargo:rustc-link-lib=static=png");

    if cargo_target_os == "macos" {
        println!("cargo:rustc-link-lib=python3.7m");
    }

    println!("cargo:rustc-link-lib=vtkcgns-9.1");
    println!("cargo:rustc-link-lib=vtkChartsCore-9.1");
    println!("cargo:rustc-link-lib=vtkCommonColor-9.1");
    println!("cargo:rustc-link-lib=vtkCommonComputationalGeometry-9.1");
    println!("cargo:rustc-link-lib=vtkCommonCore-9.1");
    println!("cargo:rustc-link-lib=vtkCommonDataModel-9.1");
    println!("cargo:rustc-link-lib=vtkCommonExecutionModel-9.1");
    println!("cargo:rustc-link-lib=vtkCommonMath-9.1");
    println!("cargo:rustc-link-lib=vtkCommonMisc-9.1");
    println!("cargo:rustc-link-lib=vtkCommonPython-9.1");
    println!("cargo:rustc-link-lib=vtkCommonSystem-9.1");
    println!("cargo:rustc-link-lib=vtkCommonTransforms-9.1");
    println!("cargo:rustc-link-lib=vtkDICOMParser-9.1");
    println!("cargo:rustc-link-lib=vtkDomainsChemistry-9.1");
    println!("cargo:rustc-link-lib=vtkDomainsChemistryOpenGL2-9.1");
    println!("cargo:rustc-link-lib=vtkdoubleconversion-9.1");
    println!("cargo:rustc-link-lib=vtkexodusII-9.1");
    println!("cargo:rustc-link-lib=vtkexpat-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersAMR-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersCore-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersExtraction-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersFlowPaths-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersGeneral-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersGeneric-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersGeometry-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersHybrid-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersHyperTree-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersImaging-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersModeling-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersParallel-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersParallelImaging-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersPoints-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersProgrammable-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersPython-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersSelection-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersSMP-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersSources-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersStatistics-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersTexture-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersTopology-9.1");
    println!("cargo:rustc-link-lib=vtkFiltersVerdict-9.1");
    println!("cargo:rustc-link-lib=vtkfmt-9.1");
    println!("cargo:rustc-link-lib=vtkfreetype-9.1");
    println!("cargo:rustc-link-lib=vtkGeovisCore-9.1");
    println!("cargo:rustc-link-lib=vtkgl2ps-9.1");
    println!("cargo:rustc-link-lib=vtkglew-9.1");
    println!("cargo:rustc-link-lib=vtkGUISupportQt-9.1");
    println!("cargo:rustc-link-lib=vtkGUISupportQtQuick-9.1");
    println!("cargo:rustc-link-lib=vtkGUISupportQtSQL-9.1");
    // println!("cargo:rustc-link-lib=vtkhdf5-9.1");
    // println!("cargo:rustc-link-lib=vtkhdf5_hl-9.1");
    println!("cargo:rustc-link-lib=vtkImagingColor-9.1");
    println!("cargo:rustc-link-lib=vtkImagingCore-9.1");
    println!("cargo:rustc-link-lib=vtkImagingFourier-9.1");
    println!("cargo:rustc-link-lib=vtkImagingGeneral-9.1");
    println!("cargo:rustc-link-lib=vtkImagingHybrid-9.1");
    println!("cargo:rustc-link-lib=vtkImagingMath-9.1");
    println!("cargo:rustc-link-lib=vtkImagingMorphological-9.1");
    println!("cargo:rustc-link-lib=vtkImagingSources-9.1");
    println!("cargo:rustc-link-lib=vtkImagingStatistics-9.1");
    println!("cargo:rustc-link-lib=vtkImagingStencil-9.1");
    println!("cargo:rustc-link-lib=vtkInfovisCore-9.1");
    println!("cargo:rustc-link-lib=vtkInfovisLayout-9.1");
    println!("cargo:rustc-link-lib=vtkInteractionImage-9.1");
    println!("cargo:rustc-link-lib=vtkInteractionStyle-9.1");
    println!("cargo:rustc-link-lib=vtkInteractionWidgets-9.1");
    println!("cargo:rustc-link-lib=vtkIOAMR-9.1");
    println!("cargo:rustc-link-lib=vtkIOAsynchronous-9.1");
    println!("cargo:rustc-link-lib=vtkIOCGNSReader-9.1");
    println!("cargo:rustc-link-lib=vtkIOChemistry-9.1");
    println!("cargo:rustc-link-lib=vtkIOCityGML-9.1");
    println!("cargo:rustc-link-lib=vtkIOCONVERGECFD-9.1");
    println!("cargo:rustc-link-lib=vtkIOCore-9.1");
    println!("cargo:rustc-link-lib=vtkIOEnSight-9.1");
    println!("cargo:rustc-link-lib=vtkIOExodus-9.1");
    println!("cargo:rustc-link-lib=vtkIOExport-9.1");
    println!("cargo:rustc-link-lib=vtkIOExportGL2PS-9.1");
    println!("cargo:rustc-link-lib=vtkIOExportPDF-9.1");
    println!("cargo:rustc-link-lib=vtkIOGeometry-9.1");
    println!("cargo:rustc-link-lib=vtkIOHDF-9.1");
    println!("cargo:rustc-link-lib=vtkIOImage-9.1");
    println!("cargo:rustc-link-lib=vtkIOImport-9.1");
    println!("cargo:rustc-link-lib=vtkIOInfovis-9.1");
    println!("cargo:rustc-link-lib=vtkIOIOSS-9.1");
    println!("cargo:rustc-link-lib=vtkIOLegacy-9.1");
    println!("cargo:rustc-link-lib=vtkIOLSDyna-9.1");
    println!("cargo:rustc-link-lib=vtkIOMINC-9.1");
    println!("cargo:rustc-link-lib=vtkIOMotionFX-9.1");
    println!("cargo:rustc-link-lib=vtkIOMovie-9.1");
    println!("cargo:rustc-link-lib=vtkIONetCDF-9.1");
    println!("cargo:rustc-link-lib=vtkIOOggTheora-9.1");
    println!("cargo:rustc-link-lib=vtkIOParallel-9.1");
    println!("cargo:rustc-link-lib=vtkIOParallelXML-9.1");
    println!("cargo:rustc-link-lib=vtkIOPLY-9.1");
    println!("cargo:rustc-link-lib=vtkIOSegY-9.1");
    println!("cargo:rustc-link-lib=vtkIOSQL-9.1");
    println!("cargo:rustc-link-lib=vtkioss-9.1");
    println!("cargo:rustc-link-lib=vtkIOTecplotTable-9.1");
    println!("cargo:rustc-link-lib=vtkIOVeraOut-9.1");
    println!("cargo:rustc-link-lib=vtkIOVideo-9.1");
    println!("cargo:rustc-link-lib=vtkIOXML-9.1");
    println!("cargo:rustc-link-lib=vtkIOXMLParser-9.1");
    // println!("cargo:rustc-link-lib=vtkjpeg-9.1");
    println!("cargo:rustc-link-lib=vtkjsoncpp-9.1");
    println!("cargo:rustc-link-lib=vtkkissfft-9.1");
    println!("cargo:rustc-link-lib=vtklibharu-9.1");
    println!("cargo:rustc-link-lib=vtklibproj-9.1");
    println!("cargo:rustc-link-lib=vtklibxml2-9.1");
    println!("cargo:rustc-link-lib=vtkloguru-9.1");
    // println!("cargo:rustc-link-lib=vtklz4-9.1");
    println!("cargo:rustc-link-lib=vtklzma-9.1");
    println!("cargo:rustc-link-lib=vtkmetaio-9.1");
    // println!("cargo:rustc-link-lib=vtknetcdf-9.1");
    println!("cargo:rustc-link-lib=vtkogg-9.1");
    println!("cargo:rustc-link-lib=vtkParallelCore-9.1");
    println!("cargo:rustc-link-lib=vtkParallelDIY-9.1");
    // println!("cargo:rustc-link-lib=vtkpng-9.1");
    println!("cargo:rustc-link-lib=vtkpugixml-9.1");
    println!("cargo:rustc-link-lib=vtkPythonContext2D-9.1");
    println!("cargo:rustc-link-lib=vtkPythonInterpreter-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingAnnotation-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingContext2D-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingContextOpenGL2-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingCore-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingFreeType-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingGL2PSOpenGL2-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingImage-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingLabel-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingLOD-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingOpenGL2-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingQt-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingSceneGraph-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingUI-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingVolume-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingVolumeOpenGL2-9.1");
    println!("cargo:rustc-link-lib=vtkRenderingVtkJS-9.1");
    println!("cargo:rustc-link-lib=vtksqlite-9.1");
    println!("cargo:rustc-link-lib=vtksys-9.1");
    println!("cargo:rustc-link-lib=vtkTestingRendering-9.1");
    // println!("cargo:rustc-link-lib=vtktheora-9.1");
    // println!("cargo:rustc-link-lib=vtktiff-9.1");
    println!("cargo:rustc-link-lib=vtkverdict-9.1");
    println!("cargo:rustc-link-lib=vtkViewsContext2D-9.1");
    println!("cargo:rustc-link-lib=vtkViewsCore-9.1");
    println!("cargo:rustc-link-lib=vtkViewsInfovis-9.1");
    println!("cargo:rustc-link-lib=vtkViewsQt-9.1");
    println!("cargo:rustc-link-lib=vtkWrappingPythonCore3.7-9.1");
    println!("cargo:rustc-link-lib=vtkWrappingTools-9.1");
    // println!("cargo:rustc-link-lib=vtkzlib-9.1");

    println!("cargo:rerun-if-changed=lib/CMakeLists.txt");
    println!("cargo:rerun-if-changed=lib/xVis.hpp");
    println!("cargo:rerun-if-changed=lib/xVis.cpp");

    println!("cargo:rerun-if-changed=src/lib.rs");
    println!("cargo:rerun-if-changed=examples/intergation/main.rs");
}
