import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtQuick.Window   2.15

import xQuick          1.0 as X
import xQuick.Controls 1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Style    1.0 as X

import VTK 9.1

X.Application {

    id: window;

    visible: true;

    VTKRenderWindow {
        id: vtkwindow
        objectName: "vtkwindow"
        anchors.fill: parent;
    }

    SplitView {

        id: rectangle;

        orientation: Qt.Horizontal

        anchors.fill: parent;

        X.Page {

            SplitView.minimumWidth: 200
            SplitView.preferredWidth: 200

            ColumnLayout {

                anchors.top: parent.top;
                anchors.topMargin: 48;
                anchors.left: parent.left;
                anchors.leftMargin: 10;
                anchors.right: parent.right;
                anchors.rightMargin: 10;

                spacing: 20;

                RowLayout {

                    Layout.fillWidth: true;

                    X.ButtonRound {
                        text: "-";
                        Layout.fillWidth: true;
                        onClicked: {
                            view.currentIndex = Math.max(0, view.currentIndex - 1);
                        }
                    }
                    X.ButtonRound {
                        text: "+";
                        Layout.fillWidth: true;
                        onClicked: {
                            view.currentIndex = Math.min(view.count -1, view.currentIndex + 1);
                        }
                    }
                }

                Button {
                    Layout.fillWidth: true;

                    text: "Push Demo Banner"

                    onClicked: X.BannerManager.show({
                        text: "Text",
                        action: "Action",
                        timeout: 3000,
                        canBeKilled: true
                    })
                }

                Button {
                    text: "Dialog test"

                    Layout.fillWidth: true;

                    onClicked: dialog.open()

                    Dialog {
                        id: dialog

                        x: (window.width - width) / 2
                        y: (window.height - height) / 2
                        parent: Overlay.overlay

                        modal: true
                        title: "Dialog"
                        standardButtons: Dialog.Yes | Dialog.No

                        Column {
                            spacing: 20
                            anchors.fill: parent
                            Label {
                                text: "Dummy question ?"
                            }
                            CheckBox {
                                text: "Do not ask again"
                                anchors.right: parent.right
                            }
                        }
                    }
                }
            }

            PageIndicator {
                id: indicator

                count: view.count
                currentIndex: view.currentIndex

                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        SwipeView {
            id: view

            interactive: false;

            clip: true;

            currentIndex: 0

            SplitView.fillHeight: true
            SplitView.fillWidth: true

            VTKRenderItem {
                objectName: "GeomView"
                renderWindow: vtkwindow

                X.ButtonRound {
                    text: "Overlay button";

                    anchors.centerIn: parent;

                    onClicked: {
                        console.log('Overlay clicked');
                    }
                }
            }

            VTKRenderItem {
                objectName: "GlyphView"
                renderWindow: vtkwindow
            }

            SplitView {

                orientation: Qt.Horizontal

                VTKRenderItem {
                    SplitView.fillHeight: true
                    SplitView.fillWidth: true
                    objectName: "GeomView2"
                    renderWindow: vtkwindow
                }

                VTKRenderItem {
                    SplitView.fillHeight: true
                    SplitView.minimumWidth: 300
                    objectName: "GlyphView2"
                    renderWindow: vtkwindow
                }
            }

            Item {

                SwipeView {

                    id: layout;

                    anchors.top: parent.top;
                    anchors.left: parent.left;
                    anchors.right: parent.right;

                    height: parent.height/2;

                    interactive: false;

                    clip: true;

                    VTKRenderItem {
                        id: geom3;
                        objectName: "GeomView3"
                        renderWindow: vtkwindow
                    }

                    VTKRenderItem {
                        id: glyph3;
                        objectName: "GlyphView3"
                        renderWindow: vtkwindow
                    }

                    Component.onCompleted:{
                        contentItem.highlightMoveDuration = 0;
                    }
                }

                X.Editor {

                    id: _editor;

                    anchors.left: parent.left;
                    anchors.right: parent.right;
                    anchors.bottom: parent.bottom;

                    height: parent.height/2;

                    contents: "
This tests font rendering within the context of shared OpenGL Context.

Using VTK 9.1 has an impact on the visual structure of the application using it.

A VTKRenderWindow element spans the bottom most surface of the application,
which implies its root element to be virtually transparent.

VTKRenderItems use it to achieve rendering, but embeded in a hierarchy of controls,
the designer has to make sure no control within this hierarchy has a non transparent
background property.

As a matter of fact, this very app, overrides the root X.Application's background
property as such:

background: Rectangle {
    color: \"#00000000\";
}

This pages mimicks gnomon's 2D/3D switching feature.

Note that using a SwipeView in this case may not be the best idea ever, but since
VTKRenderItems cannot, must not be stacked, it is the only way to achieve the expected
behavior.

The transition animation has been overloaded using:

Component.onCompleted:{
    contentItem.highlightMoveDuration = 0;
}
";

                    X.Separator {
                        anchors.top: parent.top;
                        anchors.left: parent.left;
                        anchors.right: parent.right;
                    }

                    background: Rectangle {
                        color: X.Style.baseColor;
                    }
                }

                ComboBox {

                    id: box;

                    anchors.top: parent.top;
                    anchors.topMargin: 10;
                    anchors.right: parent.right;
                    anchors.rightMargin: 10;

                    model: ["Geom", "Glyph"];

                    onCurrentTextChanged: {
                        layout.currentIndex = box.currentText == "Geom" ? 0 : 1;
                    }
                }
            }
        }
    }

    background: Rectangle {
        color: "#00000000";
    }
}
