#![recursion_limit="512"]

use cpp::cpp;
use qmetaobject::prelude::*;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////
use x_logger;
use x_gui;
use x_quick;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////
qrc!(register_resources, "/" {
    "src/main.qml",
});

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////
cpp! {{
#include <QtCore>
#include <QtQml>
#include <QtQuick>
#include <QtDebug>

#include "vtkActor.h"
#include "vtkColorTransferFunction.h"
#include "vtkConeSource.h"
#include "vtkGenericOpenGLRenderWindow.h"
#include "vtkGlyph3DMapper.h"
#include "vtkNew.h"
#include "vtkPNGWriter.h"
#include "vtkPiecewiseFunction.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkSmartVolumeMapper.h"
#include "vtkSphereSource.h"
#include "vtkTestUtilities.h"
#include "vtkTesting.h"
#include "vtkVolume.h"
#include "vtkVolumeProperty.h"
#include "vtkWindowToImageFilter.h"
#include "vtkXMLImageDataReader.h"

#include "QQuickVTKRenderItem.h"
#include "QQuickVTKRenderWindow.h"
}}
// /////////////////////////////////////////////////////////////////////////////

fn main() {

    register_resources();

    x_logger::init();
     x_quick::init();

    cpp!(unsafe [] {
        QQuickVTKRenderWindow::setupGraphicsBackend();
    });

    let conda_prefix = std::env::var("CONDA_PREFIX").unwrap_or_default();

    let mut engine = x_gui::xApplicationEngine::new();
    engine.set_application_name("x-vis Integration".into());
    engine.set_organisation_name("inria".into());
    engine.set_organisation_domain("fr".into());
    engine.add_import_path("qrc:///qml".into());
    engine.add_import_path(format!("{}/lib/qml", &conda_prefix).into());
    engine.set_property("_title".into(), engine.title().into());
    engine.set_source("qrc:///src/main.qml".into());

    // /////////////////////////////////////////////////////////////////////////

    let view = engine.view();

    cpp!(unsafe [view as "QObject *"] {

        std::function<void(QList<QObject *>)> enumerate;
        enumerate = [&enumerate] (QList<QObject *> source)
        {
            for(QObject *ob: source) {
                qInfo() << ob->objectName();
                enumerate( ob->children() );
            }
        };

        QQuickView *qiew = static_cast<QQuickView *>(view);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

        vtkNew<vtkConeSource> cone;

        vtkNew<vtkPolyDataMapper> mapper;
        mapper->SetInputConnection(cone->GetOutputPort());

        vtkNew<vtkActor> actor;
        actor->SetMapper(mapper);

        QQuickVTKRenderItem* geomItem;

        geomItem = qiew->rootObject()->findChild<QQuickVTKRenderItem *>("GeomView");
        geomItem->renderer()->AddActor(actor);
        geomItem->renderer()->ResetCamera();
        geomItem->update();

        geomItem = qiew->rootObject()->findChild<QQuickVTKRenderItem *>("GeomView2");
        geomItem->renderer()->AddActor(actor);
        geomItem->renderer()->ResetCamera();
        geomItem->update();

        geomItem = qiew->rootObject()->findChild<QQuickVTKRenderItem *>("GeomView3");
        geomItem->renderer()->AddActor(actor);
        geomItem->renderer()->ResetCamera();
        geomItem->update();

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

        vtkNew<vtkSphereSource> sphere;
        vtkNew<vtkConeSource> squad;

        vtkNew<vtkGlyph3DMapper> glyphMapper;
        glyphMapper->SetInputConnection(sphere->GetOutputPort());
        glyphMapper->SetSourceConnection(squad->GetOutputPort());
        glyphMapper->SetOrientationArray("Normals");

        vtkNew<vtkActor> glyphActor;
        glyphActor->SetMapper(glyphMapper);
        glyphActor->GetProperty()->SetDiffuseColor(0.5, 1.0, 0.8);

        QQuickVTKRenderItem* glyphItem;

        glyphItem = qiew->rootObject()->findChild<QQuickVTKRenderItem*>("GlyphView");
        glyphItem->renderer()->AddActor(glyphActor);
        glyphItem->renderer()->ResetCamera();
        glyphItem->update();

        glyphItem = qiew->rootObject()->findChild<QQuickVTKRenderItem*>("GlyphView2");
        glyphItem->renderer()->AddActor(glyphActor);
        glyphItem->renderer()->ResetCamera();
        glyphItem->update();

        glyphItem = qiew->rootObject()->findChild<QQuickVTKRenderItem*>("GlyphView3");
        glyphItem->renderer()->AddActor(glyphActor);
        glyphItem->renderer()->ResetCamera();
        glyphItem->update();
    });

    // /////////////////////////////////////////////////////////////////////////////

    engine.exec();
}
